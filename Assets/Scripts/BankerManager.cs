﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BankerManager : MonoBehaviour {


    public List<Sprite> CardSprite;
    public List<BaccaratCard> BaccaratDeck;

    public int GetDeckCount() { return BaccaratDeck.Count; }
    public int GetDeckMaxCount() { return CardSprite.Count; }

    public struct BaccaratCard { public int key; public Sprite card; }
    
    void Start() {
        ResetDeck();

        Debug.Log(PickCard().card);
    }

    /// <summary>
    /// Reset deck arrangement
    /// </summary>
    private void ResetDeck()
    {
        BaccaratDeck = new List<BaccaratCard>();

        for (int i = 0; i < CardSprite.Count; i++){
            BaccaratDeck.Add(new BaccaratCard { key = i, card = CardSprite[i] });
        }

        ShuffleDeck();
    }

    /// <summary>
    /// Shuffle newly setup deck
    /// </summary>
    public void ShuffleDeck(){

        for (int i = 0; i < BaccaratDeck.Count; i++){
            var rand = Random.Range(1, BaccaratDeck.Count);
            var tempCard = BaccaratDeck[rand];
            BaccaratDeck[rand] = BaccaratDeck[i];
            BaccaratDeck[i] = tempCard;
        }
    }

    /// <summary>
    /// Pick first index card
    /// </summary>
    /// <returns></returns>
    public BaccaratCard PickCard()
    {
        BaccaratCard bc = new BaccaratCard();
        bc = BaccaratDeck[0];
        RemovePickedCard();
        return bc;
    }

    /// <summary>
    /// Remove picked card on first index
    /// </summary>
    private void RemovePickedCard() {
        BaccaratDeck.RemoveAt(0);
    }
}
