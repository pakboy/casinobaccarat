﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public delegate void OnStateChangeHandler();

public class GameManager : MonoBehaviour
{

    public static GameManager instance = null;

    public event OnStateChangeHandler OnStateChange;
    
    void Awake()
    {

        if (instance == null)
        {
            instance = this;

            DontDestroyOnLoad(instance);
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
    }
}
