﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewBehaviourScript : MonoBehaviour {

    List<int> cardDeck;

    public void reset()
    {
        if (cardDeck == null){
            cardDeck = new List<int>();
        }
        else{
            cardDeck.Clear();
        }
    }
    public void shuffle() {
        
        for (int i = 0; i < 52; i++) {
            cardDeck.Add(i);
        }
    }
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
