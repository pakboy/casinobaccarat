﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace BLabCheminDeFerProject {

[Serializable]
	public class GameDataScriptable : ScriptableObject {
	//public Vector2[] playersCardsList = new Vector2[25];

	public string[] playersName;
	public string[] playersCountry;
	public Sprite[] playersAvatar;

	public float startingMoney = 10000;


}
}